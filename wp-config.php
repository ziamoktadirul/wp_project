<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpproject' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '15/ZVxi|{wf+,Yp)X$@!TP1y5WWtHr_K5G>9&3A;v$w*uxF<]m+Fh-eU=]aAyLpH' );
define( 'SECURE_AUTH_KEY',  'M[r9i.6:7(ZPJH$!Pmfv(*OeXZyfjTz8v),Y8mzN4bkPW<W>NoBA[}~6GxwV9|Z^' );
define( 'LOGGED_IN_KEY',    'WzSj#-R?@EsOv<$oN!rs_JwI1f)3r4T?!%0#dJ2!/{{q9OcW(cL(%$,:Q0*KJ<bF' );
define( 'NONCE_KEY',        'ik&1P(z3z-KM;cDKsg|F#u+CO3Ws_7X_JRvq}@L#u[>Y_+Q#svLhOTrQ*ac7M%*:' );
define( 'AUTH_SALT',        'kF25pmZzJn2ew39eVx[+Kmz9fpsD*b{jx>5l^lwj@m1Rqqt2(N`F:KC46M@k>8Py' );
define( 'SECURE_AUTH_SALT', 'zV.1xYa-4f]C<Q[r@p7siu0u757>qUt;]TI2z?yi}ZHE~:;coSwnq&_$J]iB{bHg' );
define( 'LOGGED_IN_SALT',   'Q[iu4$/58}k3R?9^<X$GfUIhAGZhgMF8`-+4fGW+?b65~`E2/YW,4;W{INEn0}Mg' );
define( 'NONCE_SALT',       '0EK+vFWiom)k2ds>g=y20Li)L)3Z+MvRT8L*S{jKj:x m[;N!UwR7Sf!GJ&~~$N2' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
