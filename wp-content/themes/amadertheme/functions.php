<?php



if(!function_exists('amadertheme_setup')){
	
	function amadertheme_setup(){
		add_theme_support( 'automatic-feed-links' );
		
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		
		register_nav_menus(array(
			'main-menu' => 'Main Menu',
			
		
		));
		
		register_post_type('blog', array(
			'labels' => array(
				'name' => 'blog',
				'add_new' => 'Add New Blog',
				'menu_icon' => 'el el home',
			),
			'public' => true,
			'supports' => array('title', 'editor', 'thumbnail'),
		));
		
		
		
		
		
	}
	
}
add_action('after_setup_theme', 'amadertheme_setup');




add_action('wp_enqueue_scripts', 'amadertheme_style');


function amadertheme_style(){
	
	wp_enqueue_style('bootstrap-css', get_template_directory_uri().'/assets/css/bootstrap.css');
	wp_enqueue_style('meanmenu-css', get_template_directory_uri().'/assets/css/meanmenu.css');
	wp_enqueue_style('animate-css', get_template_directory_uri().'/assets/css/animate.css');
	wp_enqueue_style('swiper-css', get_template_directory_uri().'/assets/css/swiper-bundle.css');
	wp_enqueue_style('backtotop-css', get_template_directory_uri().'/assets/css/backtotop.css');
	wp_enqueue_style('magnific-css', get_template_directory_uri().'/assets/css/magnific-popup.css');
	wp_enqueue_style('nice-css', get_template_directory_uri().'/assets/css/nice-select.css');
	wp_enqueue_style('slick-css', get_template_directory_uri().'/assets/css/slick.css');
	wp_enqueue_style('flaticon-css', get_template_directory_uri().'/assets/css/flaticon.css');
	wp_enqueue_style('awesome-css', get_template_directory_uri().'/assets/css/font-awesome-pro.css');
	wp_enqueue_style('spacing-css', get_template_directory_uri().'/assets/css/spacing.css');
	wp_enqueue_style('main-css', get_template_directory_uri().'/assets/css/main.css');
	
}




add_action('wp_enqueue_scripts', 'amadertheme_js');


function amadertheme_js(){
	
	wp_enqueue_script('waypoints-js', get_template_directory_uri().'/assets/js/vendor/waypoints.js', array('jquery'), '', true);
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/assets/js/bootstrap-bundle.js', array('jquery'), '', true);
	wp_enqueue_script('meanmenu-js', get_template_directory_uri().'/assets/js/meanmenu.js', array('jquery'), '', true);
	wp_enqueue_script('swiper-js', get_template_directory_uri().'/assets/js/swiper-bundle.js', array('jquery'), '', true);
	wp_enqueue_script('magnific-js', get_template_directory_uri().'/assets/js/magnific-popup.js', array('jquery'), '', true);
	wp_enqueue_script('parallax-js', get_template_directory_uri().'/assets/js/parallax.js', array('jquery'), '', true);
	wp_enqueue_script('backtotop-js', get_template_directory_uri().'/assets/js/backtotop.js', array('jquery'), '', true);
	wp_enqueue_script('counterup-js', get_template_directory_uri().'/assets/js/counterup.js', array('jquery'), '', true);
	wp_enqueue_script('progress-js', get_template_directory_uri().'/assets/js/js_circle-progress.min.js', array('jquery'), '', true);
	wp_enqueue_script('wow-js', get_template_directory_uri().'/assets/js/wow.js', array('jquery'), '', true);
	wp_enqueue_script('slick-js', get_template_directory_uri().'/assets/js/slick.min.js', array('jquery'), '', true);
	wp_enqueue_script('select-js', get_template_directory_uri().'/assets/js/nice-select.min.js', array('jquery'), '', true);
	wp_enqueue_script('isotope-js', get_template_directory_uri().'/assets/js/isotope-pkgd.js', array('jquery'), '', true);
	wp_enqueue_script('imagesloaded-js', get_template_directory_uri().'/assets/js/imagesloaded-pkgd.js', array('jquery'), '', true);
	wp_enqueue_script('ajax-js', get_template_directory_uri().'/assets/js/ajax-form.js', array('jquery'), '', true);
	wp_enqueue_script('main-js', get_template_directory_uri().'/assets/js/main.js', array('jquery'), '', true);
	
}


// sidebar register_nav_menus
add_action('widgets_init', 'amader_theme_sidebar');


function amader_theme_sidebar(){
	register_sidebar(array(
		'name' => __('Right Sidebar', 'amadertheme'),
		'id' => 'amader_sidebar',
		'description' => __('Right Sidebar Widget', 'amadertheme'),	
	
	));
	
	register_sidebar(array(
		'name' => __('Category Sidebar', 'amadertheme'),
		'id' => 'category_sidebar',
		'description' => __('Category Sidebar Widget', 'amadertheme'),	
		'before_widget' => '<div class="sidebar__widget sidebar__widget-content sidebar__category mb-45">',	
		'after_widget' => '</div>',	
		'before_title' => '<div class="sidebar__widget-head mb-35">
		   <h3 class="sidebar__widget-title">',
		'after_title' => '</h3></div>'
	
	));
	
	register_sidebar(array(
		'name' => __('Popularr Post Sidebar', 'amadertheme'),
		'id' => 'popularr_sidebar',
		'description' => __('Popularr Sidebar Widget', 'amadertheme'),	
		'before_widget' => ' <div class="sidebar__widget mb-45">',	
		'after_widget' => '</div>',	
		'before_title' => '<div class="sidebar__widget-head mb-35">
		   <h3 class="sidebar__widget-title">',
		'after_title' => '</h3></div>'
	
	));
	
	
	
	
	
}



// popular post widgets_init
add_action('widgets_init', 'populat_post');
function populat_post(){
	register_widget('popular_post');
}




Class popular_post extends wp_widget{
	
	public function __construct(){
		parent::__construct('popular-post-amader','Popular post', array(
			'description' => 'amader popular Post',
		));
	}
	
	public function widget($arg, $dta){
		ob_start();?>
	 <div class="sidebar__widget mb-45">
		<div class="sidebar__widget-head mb-35">
		   <h3 class="sidebar__widget-title">Popular Feeds</h3>
		</div>
		<div class="sidebar__widget-content">
		   <div class="rc__post-wrapper">
		   <?php 
		   $pp = new wp_query(array(
				'post_type' => 'post',
				'posts_per_page' => 3,
		   ));
		   
		   
		   while($pp->have_posts()): $pp->the_post();?>
			  <div class="rc__post d-flex align-items-center">
				 <div class="rc__thumb mr-20">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				 </div>
				 <div class="rc__content">
					<div class="rc__meta">
					   <span><?php the_time('F J, Y'); ?></span>
					</div>
					<h6 class="rc__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
				 </div>
			  </div>
			<?php endwhile; ?>
		   </div>
		</div>
	 </div>
		
		<?php echo ob_get_clean();
	}
	public function form($dta){
		?>
			<p>
				<label for="widget-search-2-title">Title:</label>
				<input class="widefat" id="widget-search-2-title" name="" type="text" value="">
			</p>
			<p>
				<label for="widget-search-2-title">Post Count:</label>
				<input class="widefat" id="widget-search-2-title" name="" type="number" value="">
			</p>
			<p>
				<label for="widget-search-2-title">Show Date:</label>
				<input class="widefat" id="widget-search-2-title" name="checkbox" type="checkbox" value="">
			</p>
			
		<?php 
	}
	
}