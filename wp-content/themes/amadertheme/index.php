<?php get_header(); ?>
   <main>
   <!-- Updated Breadcrumb area start -->

   <!-- Updated Breadcrumb area end -->

   <!-- News area start -->
      <section class="news__area pt-120 pb-60">
         <div class="container">
            <div class="row wow fadeInUp" data-wow-delay=".3s">
               <div class="col-xxl-9 col-xl-9 col-lg-12 col-md-12">
                  <div class="blog__main-wrapper mb-60">
                     <div class="row">
                        
						<?php 
						
							$bl = new wp_query(array(
								'post_type' => 'post',
								'post_per_page' => -1,
							
							));
						
						
						while($bl->have_posts()): $bl->the_post(); ?>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6">
                           <div class="news__singel-item mb-60">
                              <div class="news__item-thumb w-img mb-35">
                                    <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail(); ?>
                                    </a>
                                    <a class="news__tag" href="<?php the_permalink(); ?>">
									<?php 
									$category = get_the_terms(get_the_id(), 'category');
									foreach($category as $cat):?>
										<?php echo $cat->name; ?>
									<?php endforeach; ?>
									
									</a>
                              </div>
                              <div class="news__item-content">
                                    <div class="news__item-meta mb-10">
                                    <ul>
                                       <li><i class="fa-light fa-calendar-days"></i><span><?php the_time('F j, Y'); ?></span></li>
                                       <li><a href="<?php echo site_url(); ?>/author/<?php the_author(); ?>"><i class="fa-solid fa-user"></i><?php the_author(); ?></a></li>
                                    </ul>
                                    </div>
                                 <h3 class="news__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                              </div>
                           </div>
                        </div>
						<?php endwhile; ?>
                        
                        
                        
                        
                        <div class="col-12">
                           <div class="bd-basic__pagination d-flex align-items-center">
                              <nav>
                                 <ul>
                                    <li>
                                       <a href="#">
                                          <i class="fa-solid fa-arrow-left-long"></i>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">1</a>
                                    </li>
                                    <li>
                                       <span class="current">2</span>
                                    </li>
                                    <li>
                                       <a href="#">3</a>
                                    </li>
                                    <li>
                                       <a href="#">...</a>
                                    </li>
                                    <li>
                                       <a href="#">10</a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <i class="fa-solid fa-arrow-right-long"></i>
                                       </a>
                                    </li>
                                 </ul>
                              </nav>
                           </div>
                     </div>
                     </div>
                  </div>
               </div>
              
                  <?php get_sidebar(); ?>
              
            </div>
         </div>
      </section>
   <!-- News area end -->
   
   </main>
   <!-- Body main wrapper end -->

   <!-- Footer area start -->

<?php get_footer(); ?>