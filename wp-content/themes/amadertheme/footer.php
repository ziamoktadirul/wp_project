   <footer>
      <section class="footer__area footer-bg pt-95">
         <div class="container">
            <div class="footer-main__wrapper pb-35">
               <div class="row">
                  <div class="col-xxl-3 col-xl-3 col-lg-4 col-md-6 col-sm-6">
                     <div class="footer__widget footer-col-1 mb-60">
                        <div class="footer__title">
                           <h4>About Us</h4>
                        </div><div class="footer__content">
                           <p>Learn from industry-leading speakers
                              like Romain Guy and Chet Haase who
                              have been building Android since 1.0,
                              Jhey Tompkins and Una Kravets from
                              the Google Chrome team.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xxl-3 col-xl-3 col-lg-4 col-md-6 col-sm-6">
                     <div class="footer__widget footer-col-2 mb-60">
                        <div class="footer__title">
                           <h4>Pages</h4>
                        </div>
                        <div class="footer__link">
                           <ul>
                              <li><a href="#">About Us</a></li>
                              <li><a href="#"> Our Terms</a></li>
                              <li><a href="#">Services</a></li>
                              <li><a href="#">Membership</a></li>
                              <li><a  href="#">Price & Plan</a></li>
                              <li><a href="#">News</a></li>
                              <li><a href="#">Refund</a></li>
                              <li><a href="#">Get In Touch</a></li>
                              <li class="custom__link"><a  href="#">Our Team</a></li>
                              <li class="custom__link"><a href="#">Price & Plan</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-xxl-2 col-xl-3 col-lg-4 col-md-6 col-sm-6">
                     <div class="footer__widget footer-col-3 mb-60">
                        <div class="footer__title">
                           <h4>Our Services</h4>
                        </div>
                        <div class="footer__link-2">
                           <ul>
                              <li><a href="#">Software Solution</a></li>
                              <li><a href="#">Digital Marketing</a></li>
                              <li><a href="#">UI & UX Design</a></li>
                              <li><a href="#">Web Development</a></li>
                              <li class="custom__link"><a href="#">24/7 Online Support</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-xxl-3 col-xl-3 offset-xxl-1 col-lg-4 col-md-6 col-sm-6">
                     <div class="footer__widget footer-col-4 mb-60">
                        <div class="footer__title">
                           <h4>Our Availability</h4>
                        </div>
                        <div class="footer__contact">
                           <div class="footer__contact-item">
                              <span>Mon - Fri</span>
                              <p class="mb-20">10:00 am to 06:00 pm</p>
                           </div>
                           <div class="footer__contact-item">
                              <span>Saturday (1st & 4th)</span>
                              <p class="mb-25">08:00 am to 04:00 pm</p>
                           </div>
                           <div class="footer__support">
                              <div class="footer__support-inner">
                                 <div class="footer__support-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/footet/massege.png" alt="image not found">
                                 </div>
                                 <div class="footer__support-title">
                                    <span>Emergency Service</span>
                                    <a href="tel:00011122233">000 111 222 33</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="footer__info-area">
                     <div class="row align-items-center">
                        <div class="col-md-6 col-sm-6">
                           <div class="footer__logo">
                              <a href="index.html">
                                 <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/logo-2.png" alt="logo not found">
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                           <div class="footer__link-3 text-md-end">
                              <ul>
                                 <li><a href="faq.html">faq</a></li>
                                 <li><a href="about.html">careers</a></li>
                                 <li><a href="contact.html">contact</a></li>
                              </ul>                             
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer__copyright">
            <div class="container">
               <div class="copyright__text">
                  <p>Copyright & design by <a target="_blank" href="#"> @Bdevs</a> - 2022</p>
               </div>
            </div>
         </div>
      </section>
   </footer>
   <!-- Footer area end -->
   
   <!-- Back to top start -->
   <div class="progress-wrap">
      <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
         <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
      </svg>
   </div>
   <!-- Back to top end -->
   
      <!-- JS here -->
     
  
	  <?php wp_footer(); ?>
   </body>

</html>