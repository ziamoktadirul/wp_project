<!doctype html>
<html <?php language_attributes(); ?>>

<head>
      <meta charset="<?php bloginfo('charset'); ?>">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      
      <meta name="description" content="<?php bloginfo('name'); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Place favicon.ico in the root directory -->
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png">
      <!-- CSS here -->
     
	  <?php wp_head(); ?>
   </head>
   <body>
      <!--[if lte IE 9]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
      <![endif]-->
      <!-- Preloader start -->
      <div class="preloader">
         <div class="loader rubix-cube">
            <div class="layer layer-1"></div>
            <div class="layer layer-2"></div>
            <div class="layer layer-3 color-1"></div>
            <div class="layer layer-4"></div>
            <div class="layer layer-5"></div>
            <div class="layer layer-6"></div>
            <div class="layer layer-7"></div>
            <div class="layer layer-8"></div>
         </div>
      </div>
      <!-- Preloader end -->
      <!-- Offcanvas area start -->
      <div class="fix">
         <div class="offcanvas__info">
            <div class="offcanvas__wrapper">
               <div class="offcanvas__content">
                  <div class="offcanvas__top mb-40 d-flex justify-content-between align-items-center">
                     <div class="offcanvas__logo">
                        <a href="index.html">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/ud-white-logo.png" alt="logo not found">
                        </a>
                     </div>
                     <div class="offcanvas__close">
                        <button>
                        <i class="fal fa-times"></i>
                        </button>
                     </div>
                  </div>
                  <div class="offcanvas__search mb-25">
                     <form action="#">
                        <input type="text" placeholder="What are you searching for?">
                        <button type="submit" ><i class="far fa-search"></i></button>
                     </form>
                  </div>
                  <div class="mobile-menu fix mb-40"></div>
                  <div class="offcanvas__contact mt-30 mb-20">
                     <h4>Contact Info</h4>
                     <ul>
                        <li class="d-flex align-items-center">
                           <div class="offcanvas__contact-icon mr-15">
                              <i class="fal fa-map-marker-alt"></i>
                           </div>
                           <div class="offcanvas__contact-text">
                              <a target="_blank" href="https://www.google.com/maps/place/Dhaka/@23.7806207,90.3492859,12z/data=!3m1!4b1!4m5!3m4!1s0x3755b8b087026b81:0x8fa563bbdd5904c2!8m2!3d23.8104753!4d90.4119873">12/A, Mirnada City Tower, NYC</a>
                           </div>
                        </li>
                        <li class="d-flex align-items-center">
                           <div class="offcanvas__contact-icon mr-15">
                              <i class="far fa-phone"></i>
                           </div>
                           <div class="offcanvas__contact-text">
                              <a href="tel:+088889797697">+088889797697</a>
                           </div>
                        </li>
                        <li class="d-flex align-items-center">
                           <div class="offcanvas__contact-icon mr-15">
                              <i class="fal fa-envelope"></i>
                           </div>
                           <div class="offcanvas__contact-text">
                              <a href="tel:+012-345-6789"><span class="mailto:support@mail.com">support@mail.com</span></a>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <div class="offcanvas__social">
                     <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="offcanvas-overlay"></div>
      <div class="offcanvas-overlay-white"></div>
      <!-- Offcanvas area start -->
      <!-- Updated Header Area Start -->
      <header>
         <div class="bd-ud-header-area">
            <div class="bd-ud-header-middle ud-white-bg d-none d-lg-block pt-25 pb-25">
               <div class="container">
                  <div class="row align-items-center">
                     <div class="col-lg-3">
                        <div class="bd-ud-logo">
                           <a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/ud-logo.png" alt="bd-ud-logo"></a>
                        </div>
                     </div>
                     <div class="col-lg-9">
                        <div class="bd-ud-header-middle-info">
                           <ul>
                              <li><span>Are you ready to grow up your business?</span> <a href="contact.html">Contact us</a></li>
                              <li><i class="fa-regular fa-clock"></i> <b> Openning Time:</b> <span> Mon - Fri: 8.00 am - 6.00 pm</span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="bd-ud-header-sticky" class="bd-ud-header-bottom bd-ud-hm-padd ud-black-bg-1">
               <div class="container">
                  <div class="row align-items-center">
                     <div class="col-xl-7 col-lg-9 col-4">
                        <div class="bd-ud-main-menu d-none d-lg-block">
                           <nav id="mobile-menu">
						   <?php 
							wp_nav_menu(array(
								'theme_location' => 'main-menu',
								'menu-class' => 'menu-item-has-children',
							));
						   ?>
                              
                           </nav>
                        </div>
                        <div class="bd-ud-mobile-logo d-lg-none">
                           <a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/ud-white-logo.png" alt="mobile logo"></a>
                        </div>
                     </div>
                     <div class="col-xl-5 col-lg-3 col-8">
                        <div class="bd-ud-header-bottom-cta">
                           <div class="bd-ud-header-bottom-cta-wrapper d-flex align-items-center justify-content-end">
                              <div class="bd-ud-header-bottom-cta-item d-none d-sm-block">
                                 <div class="bd-ud-search__button p-relative">
                                    <a class="bd-ud-search__toggle" href="javascript:void(0)">
                                    <i class="bd-ud-header-search__btn fa-regular fa-magnifying-glass"></i>
                                    <i class="bd-ud-header-search__close fa-regular fa-xmark"></i>
                                    </a>
                                    <div class="bd-search__form p-relative">
                                       <form action="#">
                                          <input type="text" placeholder="Search here...">
                                          <button type="submit">
                                          <i class="fa-regular fa-magnifying-glass"></i>
                                          </button>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <div class="bd-ud-header-bottom-cta-item">
                                 <div class="bd-ud-header-bottom-cart">
                                    <a href="#"><i class="flaticon-shopping-cart"></i></a>
                                 </div>
                              </div>
                              <div class="bd-ud-header-bottom-cta-item d-none d-xl-block">
                                 <a class="bd-ud-btn" href="contact.html">GET APPOINTMRNT<i class="fa-sharp fa-solid fa-chevron-right"></i></a>
                              </div>
                              <div class="bd-ud-header-bottom-cta-item">
                                 <div class="bd-ud-header__toggle">
                                    <button class="sidebar__toggle">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/svg/hambergar.svg" alt="hambergar">
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <!-- Updated Header Area End -->
	     <div class="bd-ud-breadcrumb__area bg-css" data-background="<?php echo get_template_directory_uri(); ?>/assets/img/bg/ud-breadcrumb.jpg">
      <div class="bd-ud-breadcrumb-shape-1 p-absolute w-img">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/shape/breadcrumb-shape-1.png" alt="breadcrumb-shape-1">
      </div>
      <div class="bd-ud-breadcrumb-shape-2 p-absolute w-img">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/shape/breadcrumb-shape-2.png" alt="breadcrumb-shape-1">
      </div>
      <div class="container">
         <div class="row">
            <div class="col-xl-12">
               <div class="bd-ud-breadcrumb__wrapper">
			   <?php if(is_home()): ?>
                  <div class="bd-ud-breadcrumb__title">
                     <h2><?php bloginfo('name'); ?></h2>
                  </div>
				  <?php else: ?>
				  <div class="bd-ud-breadcrumb__title">
                     <h2><?php the_title(); ?></h2>
                  </div>
                  <div class="bd-ud-breadcrumb__menu">
                     <nav aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
                        <ul class="trail-items">
                           <li class="trail-item trail-begin"><span><a href="<?php echo home_url(); ?>">Eurtech</a></span></li>
                           <li class="trail-item trail-end"><span><?php the_title(); ?></span></li>
                        </ul>
                     </nav>
                  </div>
				  <?php endif; ?>
               </div>
            </div>
         </div>
      </div>
   </div>